    function data() {
      var data=new Date();
      var hila=data.getMonth();
      switch(hila) {
        case 0:
          hila="urtarrilak, ";
          break;    
        case 1:
          hila="otsailak, ";
          break;
        case 2:
          hila="martxoak, ";
          break;    
        case 3:
          hila="apirilak, ";
          break;
        case 4:
          hila="maiatzak, ";
          break;    
        case 5:
          hila="ekainak, ";
          break;
        case 6:
          hila="uztailak, ";
          break;    
        case 7:
          hila="abuztuak, ";
          break;
        case 8:
          hila="irailak, ";
          break;    
        case 9:
          hila="urriak, ";
          break;
        case 10:
          hila="azaroak, ";
          break;    
        case 11:
          hila="abenduak, ";
          break;
        default:
          hila="desconocido"
          break;
      }
      var hour=data.getHours()
      if (hour < 12) { a_p = "AM"; } else { a_p = "PM"; }
      if (hour == 0) { hour = 12; }
      if (hour > 12) { hour = hour - 12; }
      var minu = data.getMinutes();
      minu = minu + "";
      if (minu.length == 1) { minu = "0" + minu; }
      data=data.getFullYear()+"ko "+hila+data.getDate()+". "+hour+":"+minu+"&nbsp;"+a_p+"&nbsp;&nbsp;&nbsp;eu / <a class='grey' href='/esp' title='erderaz'>es</a>";
      document.getElementById('fhi').innerHTML=data;
    }

