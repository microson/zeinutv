#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use HTML::Entities;
use Encode;
use JSON;

my ($text, $hidden, $json)='';
my %contenido = ();
my $cgi = CGI->new();
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
);

# Mandar el Content-Type es obligatorio
print $cgi->header(
                -charset=>'UTF-8',
                -type=>'application/json',
                -expire=>'now');
# Get data for selected new
my $result = $dbh->selectrow_hashref("SELECT date,tites,titeu,deses,deseu,txtes,txteu,id_cat,ticpar FROM videos WHERE id=?",undef,$cgi->param('id'));
undef $dbh;

# Create JSON structure to send it to the browser
my $tites = decode_entities(decode('UTF-8',$result->{tites}),'\W');
$tites =~ s/\'/\\'/g;
$tites =~ s/\"/\\"/g;
my $titeu = decode_entities(decode('UTF-8',$result->{titeu}),'\W');
$titeu =~ s/\'/\\'/g;
$titeu =~ s/\"/\\"/g;
my $deses = decode_entities(decode('UTF-8',$result->{deses}),'\W');
$deses =~ s/\'/\\'/g;
$deses =~ s/\"/\\"/g;
my $deseu = decode_entities(decode('UTF-8',$result->{deseu}),'\W');
$deseu =~ s/\'/\\'/g;
$deseu =~ s/\"/\\"/g;
my $txtes = decode_entities(decode('UTF-8',$result->{txtes}),'\W');
$txtes =~ s/\'/\\'/g;
$txtes =~ s/\"/\\"/g;
my $txteu = decode_entities(decode('UTF-8',$result->{txteu}),'\W');
$txteu =~ s/\'/\\'/g;
$txteu =~ s/\"/\\"/g;
%contenido = (
  'id' => $cgi->param('id'),
  'date' => substr($result->{date},8,2)."-".substr($result->{date},5,2)."-".substr($result->{date},0,4),
  'tites' => $tites,
  'titeu' => $titeu,
  'deses' => $deses,
  'deseu' => $deseu,
  'txtes' => $txtes,
  'txteu' => $txteu,
  'ticpar' => $result->{ticpar},
  'id_cat' => $result->{id_cat}
);
$json = encode_json \%contenido;
print $json;
