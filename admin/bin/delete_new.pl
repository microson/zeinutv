#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use File::Remove 'remove';

my $path = "../../videos/berriak/";
my $cgi = CGI->new();
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
);

# Mandar el Content-Type es obligatorio
print $cgi->header(
		-charset=>'UTF-8',
		-type=>'text/html');

if ( $cgi->param('id') =~ /^\d{2}-\d{2}-\d{4}_es/ ) {
  if ( -e $path.$cgi->param('id')."_es.m4v" ) { remove ($path.$cgi->param('id')."_es.png"); }
  if ( -e $path.$cgi->param('id')."_eu.m4v" ) { remove ($path.$cgi->param('id')."_eu.png"); }
} else {
  $dbh->begin_work;
  $dbh->do("DELETE FROM videos WHERE id='".$cgi->param('id')."'");

  if ( -e $path.$cgi->param('id')."_es.png" ) { remove ($path.$cgi->param('id')."_es.png"); }
  if ( -e $path.$cgi->param('id')."_eu.png" ) { remove ($path.$cgi->param('id')."_eu.png"); }
  if ( -e $path.$cgi->param('id')."_es.m4v" ) { remove ($path.$cgi->param('id')."_es.m4v");remove ($path.$cgi->param('id')."_es.webm"); }
  if ( -e $path.$cgi->param('id')."_eu.m4v" ) { remove ($path.$cgi->param('id')."_eu.m4v");remove ($path.$cgi->param('id')."_eu.webm"); }

$dbh->commit;
undef $dbh;
}
