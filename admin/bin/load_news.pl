#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use HTML::Entities;
use Encode;

my $cgi = CGI->new();
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
);

my $date = $cgi->param('date');

# Mandar el Content-Type es obligatorio
print $cgi->header(
		-charset=>'UTF-8',
		-type=>'text/html');

# Count grid news
my $sth_tmp = $dbh->prepare("SELECT COUNT(id) FROM videos WHERE ticpar='si' AND date='".$date."'");
$sth_tmp->execute();
my ($rows1)=$sth_tmp->fetchrow_array();
$sth_tmp->finish;

# Count simple news
$sth_tmp = $dbh->prepare("SELECT COUNT(id) FROM videos WHERE ticpar='no' AND date='".$date."'");
$sth_tmp->execute();
my ($rows2)=$sth_tmp->fetchrow_array();
$sth_tmp->finish;

# Find resume video for selected day
$date = substr($cgi->param('date'),8,2)."-".substr($cgi->param('date'),5,2)."-".substr($cgi->param('date'),0,4);
print "<div class='section'><span class='info'>¿Vídeo resumen? : </span>";
if ( -e "../../videos/berriak/".$date."_es.m4v" ) {
  print "sí. <a onclick='return ask(\"".$date."\",\"video\");' href='javascript:ajaxFunction(\"delete_new\");' title='eliminar resumen'>Eliminar</a>";
} else {
  print "no.";
}
print "</div>";

my $i=1;

# Show results for grid news
if ($rows1 != 0) {
  my $sth = $dbh->prepare("SELECT id,tites,deses FROM videos WHERE ticpar='si' AND date='".$cgi->param('date')."' ORDER BY grid_order");
  $sth->execute();
  print "<div class='section'><span class='info'>Noticias en parrilla : </span>$rows1</div>";
  while (my $result = $sth->fetchrow_hashref()) {
    print "<div id='new_".$result->{id}."'";
    print "<div class='flota'>";
    print "<img src='/videos/berriak/".$result->{id}."_es.png' alt='Imagen de la noticia' />";
    print "</div>";
    print "<div class='new'>";
    print "<span class='tit'>".decode_entities($result->{tites},'\W')."</span><br />";
    print $result->{deses}."<br />";
    print "<a onclick='return set(".$result->{id}.");' href='javascript:ajaxFunction(\"edit_show_data\");' title='editar noticia'>Editar</a> <a onclick='return ask(".$result->{id}.",\"video\");' href='javascript:ajaxFunction(\"delete_new\");' title='eliminar noticia'>Eliminar</a><br />";
    # Show up/down actions but only with more than one result.
    if ($rows1 != 1) {
      if ($i == 1) {
        print "<a style='visibility:hidden'>Subir / </a><a onclick='return set(".$result->{id}.");' href='javascript:ajaxFunction(\"movedown_new\");' title='bajar noticia'>Bajar</a>";
      } elsif ($i == $rows1) {
        print "<a onclick='return set(".$result->{id}.");' href='javascript:ajaxFunction(\"moveup_new\");' title='subir noticia'>Subir</a><a style='visibility:hidden'> / Bajar</a>";
      } else {
	print "<a onclick='return set(".$result->{id}.");' href='javascript:ajaxFunction(\"moveup_new\");' title='subir noticia'>Subir</a> / <a onclick='return set(".$result->{id}.");' href='javascript:ajaxFunction(\"movedown_new\");' title='bajar noticia'>Bajar</a>";
      }
    }
    print "</div>";
    print "</div><div class='sepa'></div>";
    $i++;
  }
  $sth->finish;
} else {
  print "<div class='section'><span class='info'>No hay noticias en parrilla.</span>";
  print "</div><div class='sepa'></div>";
}

# Show results for simple news
if ($rows2 != 0) {
  my $sth = $dbh->prepare("SELECT id,tites,deses FROM videos WHERE ticpar='no' AND date='".$cgi->param('date')."' ORDER BY id DESC");
  $sth->execute();
  print "<div class='section'><span class='info'>Noticias sencillas : </span>$rows2</div>";
  while (my $result = $sth->fetchrow_hashref()) {
    print "<div id='new_".$result->{id}."'";
    print "<div class='flota'>";
    print "<img src='/videos/berriak/".$result->{id}."_es.png' alt='Imagen de la noticia' />";
    print "</div>";
    print "<div class='new'>";
    print "<span class='tit'>".substr($result->{tites},0,20)."...</span><br />";
    print substr($result->{deses},0,25)."...<br />";
    print "<a onclick='return ask(".$result->{id}.",\"video\");' href='javascript:ajaxFunction(\"delete_new\");' title='eliminar noticia'>Eliminar</a>";
    print "</div>";
    print "</div><div class='sepa'></div>";
  }
  $sth->finish;
} else {
  print "<div class='section'><span class='info'>No hay noticias sencillas.</span>";
  print "</div><div class='sepa'></div>";
}
undef $dbh;
