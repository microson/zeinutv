#!/usr/bin/perl

use strict;
use DBI;
use DateTime;
use DateTime::Format::Strptime;

my $path="../../videos/berriak/";
my @loop=();
my $parser = DateTime::Format::Strptime->new( pattern => '%Y-%m-%d');
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
) or die "Can't connect to database: $DBI::errstr";
my $dt = DateTime->now;
my $purge = $dt->subtract(days => 31)->ymd;

# Backup all data
qx(rm -rf ../../videos/berriak.suyccom.bak/);
qx(rm -f ../db/ztv.sqlite3.suyccom.bak);
qx(cp -p ../db/ztv.sqlite3 ../db/ztv.sqlite3.suyccom.bak);

qx(cp -rp $path ../../videos/berriak.suyccom.bak/);


# Delete videos from FS (but not Comunidad Sorda).
my $sth = $dbh->prepare("SELECT id FROM videos WHERE date <= '".$purge."' AND id_cat != 6") or die "Can't prepare SELECT: $DBI::errstr";
$sth->execute() or die "Can't execute SELECT: $DBI::errstr";
while (my $id = $sth->fetchrow_array()) {
  my $temp = $path.$id."_*";
  qx(rm $temp);
}
$sth->finish;

# Get the oldest date from DB
my $sth = $dbh->prepare("SELECT MIN(date) FROM videos") or die "Can't prepare SELECT MIN: $DBI::errstr";
$sth->execute() or die "Can't execute SELECT MIN: $DBI::errstr";
my ($oldest) = $sth->fetchrow_array();
$sth->finish;

# Delete old videos from DB
my $sth = $dbh->do("DELETE FROM videos WHERE date <= '".$purge."' AND id_cat != 6") or die "Can't DELETE: $DBI::errstr";

undef $dbh;

# Delete resume videos day by day from FS
my $start_dt = DateTime->now->subtract(days => 31);
my $end_dt   = $parser->parse_datetime($oldest);

my @list = ();
for (my $dt = $start_dt->clone(); $dt >= $end_dt; $dt->subtract(days => 1) ) {
  if (-f $path.$dt->clone()->dmy."_es.m4v" || -f $path.$dt->clone()->dmy."_eu.m4v") {
    my  $temp = $path.$dt->clone()->dmy."_*";
    qx(rm $temp);
  }
}
