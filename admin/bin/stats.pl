#!/usr/bin/perl

use strict; 
use DBI;
use CGI;
use HTML::Template;
use DateTime;
use File::Find;

my $path = "../../videos/berriak/";
my $cgi = CGI->new();
my $template = HTML::Template->new(filename => "../tmpl/stats.tmpl");
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
) or die "Can't connect to database: $DBI::errstr";

# Show the Content-Type is mandatory
print $cgi->header(
                -charset=>'UTF-8',
                -type=>'text/html');

# STATS: Number of news on DB
my $sth = $dbh->prepare("SELECT COUNT(id) FROM videos") or die "Can't prepare SELECT COUNT: $DBI::errstr";
$sth->execute() or die "Can't execute SELECT COUNT: $DBI::errstr";
my ($rows)= $sth->fetchrow_array();
$sth->finish;
$template->param(total_news=>$rows);

# STATS: Size of news files
my $size = 0;
find(sub { $size += -s $_ if -f $_ }, $path);
$size = ($size / 1024)/1024;
$size = sprintf("%.1f",$size);
$template->param(total_size=>$size);

# MAINT: General info
$template->param(today => DateTime->now->dmy, start_dt => DateTime->now->subtract(days => 31)->dmy);

# Disconnect from DB
undef $dbh;

# Show the template
print $template->output;
