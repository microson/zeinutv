#!/usr/bin/perl

use strict;
use DBI;
use File::Copy qw(copy);
use autodie qw(chmod copy);
use HTML::Template;
use HTML::Entities;
use CGI;
use Encode;

my $contenido="";
my $errores = "";
my $template = "";
my $path="../../videos/berriak/";
my $cgi = CGI->new();
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
);

# Mandar el Content-Type es obligatorio
print $cgi->header(
		-charset=>'UTF-8',
		-type=>'text/html');

# Set date for today
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
$year += 1900;
$mon += 01;
if ($mon <= 9) { $mon = "0$mon"; }
if ($mday <= 9) { $mday = "0$mday"; }
my $date = "$mday-$mon-$year";

# Action INDEX o sin parámetro "what".
if ($cgi->param('what') eq 'index' || !$cgi->param('what')) {
  $template = HTML::Template->new(filename => "../tmpl/add_vid.tmpl");
  # Obtain categories.
  my @loop = ();
  my $sth = $dbh->prepare("SELECT id,name FROM categories");
  $sth->execute();
  while(my $result = $sth->fetchrow_hashref()) {
    my %row = (id_cat => $result->{id}, text => $result->{name});
    push(@loop, \%row);
  }
  $sth->finish;

  $template->param(date=>$date, cats=>\@loop);

# Action ADD.
} elsif ($cgi->param('what') eq 'add') {
  $template = HTML::Template->new(filename => "../tmpl/results_vid.tmpl");
  # At very first, we deny a new if it exists with the same tites and deseu
  if ($cgi->param('ticres') ne 'ticres') {
    # Convert chars and 'entitize' it.
    my $dectites = encode_entities(decode('UTF-8', $cgi->param('tites')),'\W');
    my $decdeseu = encode_entities(decode('UTF-8', $cgi->param('deseu')),'\W');
    my $sth = $dbh->prepare("SELECT id FROM videos WHERE tites = '$dectites' AND deseu = '$dectites'");
    $sth->execute();
    my ($rows)= $sth->fetchrow_array();
    if ($rows != 0) { 
      $errores = "Ya existe esta notic&iacute;a.";
    }
    $sth->finish;
  }
  if ($errores eq '') {
    # Verificamos que los campos obligatorios están.
    if ($cgi->param('tites') eq '') {
      $errores.="* El título en castellano es obligatorio.<br />";
    }
    if ($cgi->param('titeu') eq '') {
      $errores.="* El título en euskera es obligatorio.<br />";
    }
    if ($cgi->param('deses') eq '') {
      $errores.="* La descripción en castellano es obligatoria.<br />";
    }
    if ($cgi->param('deseu') eq '') {
      $errores.="* La descripción en euskera es obligatoria.<br />";
    }
    if ($cgi->param('txtes') eq '') {
      $errores.="* El texto en castellano es obligatorio.<br />";
    }
    if ($cgi->param('txteu') eq '') {
      $errores.="* El texto en castellano es obligatorio.<br />";
    }
    if ($cgi->param('date') eq '') {
      $errores.="* La fecha es obligatoria (DD-MM-AAAA).<br />";
    }
    if ($cgi->param('date') !~ m/^\d{2}\-\d{2}\-\d{4}$/) {
      $errores.="* La fecha no tiene el formato correcto.<br />";
    }
    if ($cgi->upload('file_es') eq '') {
      $errores.="* Debes elegir un fichero de vídeo en castellano.<br />";
    }
    if ($cgi->upload('file_eu') eq '') {
      $errores.="* Debes elegir un fichero de vídeo en euskera.<br />";
    }
  }
  # Si no hay problema con los campos procesamos el vídeo.
  if ($errores eq '') {
    binmode($cgi->upload('file_es'));
    binmode($cgi->upload('file_eu'));
    my $id_vid=$dbh->func('last_insert_rowid');
    # Filtramos los vídeos resumen
    if ($cgi->param('ticres') eq 'ticres') {
      my $name_video_resumen = $path.$cgi->param('date');
      # Upload es
      copy($cgi->upload('file_es'), $name_video_resumen.'_es.m4v');
      qx(ffmpeg -i $name_video_resumen"_es.m4v" -cpu-used 5 -threads 8 -b:v 600k $name_video_resumen"_es.webm" 2>&1);
      chmod 0666, $name_video_resumen.'_es.m4v';
      chmod 0666, $name_video_resumen.'_es.webm';
      # Upload eu
      copy($cgi->upload('file_eu'), $name_video_resumen.'_eu.m4v');
      qx(ffmpeg -i $name_video_resumen"_eu.m4v" -cpu-used 5 -threads 8 -b:v 600k $name_video_resumen"_eu.webm" 2>&1);
      chmod 0666, $name_video_resumen.'_eu.m4v';
      chmod 0666, $name_video_resumen.'_eu.webm';
  
      # Mostrar resultados
      $template->param(contenido=>"Has a&ntilde;adido el video resumen del ".$cgi->param('date'));
    } else {
      # Escape all the text.
      my $dectites = encode_entities(decode('UTF-8', $cgi->param('tites')),'\W');
      my $dectiteu = encode_entities(decode('UTF-8', $cgi->param('titeu')),'\W');
      my $decdeses = encode_entities(decode('UTF-8', $cgi->param('deses')),'\W');
      my $decdeseu = encode_entities(decode('UTF-8', $cgi->param('deseu')),'\W');
      my $dectxtes = encode_entities(decode('UTF-8', $cgi->param('txtes')),'\W');
      my $dectxteu = encode_entities(decode('UTF-8', $cgi->param('txteu')),'\W');

      # Filtramos las noticias de la parrilla
      my $ticpar = 'no';
      if ($cgi->param('ticpar') eq 'ticpar') {
        $ticpar = 'si';
      }
      # Guardamos en BD
      $dbh->begin_work;
      $dbh->do("INSERT INTO videos (tites, titeu, deses, deseu, txtes, txteu, id_cat, date, ticpar, grid_order) VALUES ('".$dectites."', '".$dectiteu."', '".$decdeses."', '".$decdeseu."', '".$dectxtes."', '".$dectxteu."', '".$cgi->param('cats')."', '".substr($cgi->param('date'),6,4)."-".substr($cgi->param('date'),3,2)."-".substr($cgi->param('date'),0,2)."', '".$ticpar."', '".$cgi->param('nn')."')");
      
      # Upload es
      $id_vid=$dbh->func('last_insert_rowid');
      my $new_filename=$path.$id_vid.'_es.m4v';
      copy($cgi->upload('file_es'), $new_filename);
      qx(ffmpeg -i $new_filename -cpu-used 5 -threads 8 -b:v 600k $path$id_vid"_es.webm" 2>&1);
      chmod 0666, $new_filename;
      chmod 0666, $path.$id_vid.'_es.webm';
      # Duración del vídeo (misma para es y eu)
      my $duration=qx(ffmpeg -i $new_filename 2>&1| awk '\$1 == "Duration:" {print \$2}');
      $duration=substr($duration,3,5);
      if (substr($duration,0,1) eq "0") { $duration=substr($duration,1,4)}
      # Thumbnail es
      qx(ffmpeg -y -i $new_filename -vcodec png -vframes 1 -an -ss 00:00:08 -f rawvideo -s 144x82 $path$id_vid"_es".png 2>&1);
      chmod 0666, $path.$id_vid.'_es.png';

      # Upload eu
      $id_vid=$dbh->func('last_insert_rowid');
      $new_filename=$path.$id_vid.'_eu.m4v';
      copy($cgi->upload('file_eu'), $new_filename);
      qx(ffmpeg -i $new_filename -cpu-used 5 -threads 8 -b:v 600k $path$id_vid"_eu.webm" 2>&1);
      chmod 0666, $new_filename;
      chmod 0666, $path.$id_vid.'_eu.webm';
      # Thumbnail eu
      qx(ffmpeg -y -i $new_filename -vcodec png -vframes 1 -an -ss 00:00:08 -f rawvideo -s 144x82 $path$id_vid"_eu".png 2>&1);
      chmod 0666, $path.$id_vid.'_eu.png';
 
      $dbh->commit;

      # Guardamos la duración del vídeo
      $dbh->begin_work;
      $dbh->do("UPDATE videos SET duration='".$duration."' WHERE id=".$id_vid."");

      $dbh->commit;
      
      # Show category at the result tmpl
      my $result = $dbh->selectrow_hashref("SELECT name FROM categories WHERE id=?",undef,$cgi->param('cats'));
      undef $dbh;

      # Mostrar resultados
      $template->param(
	titulo=>$cgi->param('tites'),
	categoria=>$result->{name},
	duracion=>$duration,
	parrilla=>$ticpar
      );
    }

  # Mostramos los errores
  } else {
    $errores='<p>El fichero de v&iacute;deo no se ha guardado : '.$errores.'</p>';
    $template->param(errores=>$errores);
  }
}

# Desconectamos de la base de datos
undef $dbh;

# Mostramos la plantilla
print $template->output;
