#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use HTML::Entities;
use Encode;

my $cgi = CGI->new();
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
);

my $id_cat=$cgi->param('id_cat');
$id_cat++;
my $date = substr($cgi->param('date'),6,4)."-".substr($cgi->param('date'),3,2)."-".substr($cgi->param('date'),0,2);

# Grid order is relevant in case of 'no' and if there is a date change
my $grid = $cgi->param('ticpar');
if ($grid eq 'si') {
  my $result=$dbh->selectrow_hashref("SELECT date FROM videos WHERE id=?",undef,$cgi->param('id'));
  if ($date ne $result->{date}) {
    my $sth=$dbh->prepare("SELECT MAX(grid_order) FROM videos WHERE date='".$date."'");
    $sth->execute();
    my @max = $sth->fetchrow_array();
    $max[0]++;
    if ($max[0]>0) {
      $grid=" ,ticpar='si', grid_order=$max[0]";
    } else {
      $grid=" ,ticpar='si', grid_order='1'";
    }
  } else {
    $grid='';
  }
} elsif ($grid eq 'no') {
  $grid=" ,ticpar='no', grid_order=0";
}

# Mandar el Content-Type es obligatorio
print $cgi->header(
                -charset=>'UTF-8',
                -type=>'text/html');

my $dectites = encode_entities(decode('UTF-8', $cgi->param('tites')),'\W');
my $dectiteu = encode_entities(decode('UTF-8', $cgi->param('titeu')),'\W');
my $decdeses = encode_entities(decode('UTF-8', $cgi->param('deses')),'\W');
my $decdeseu = encode_entities(decode('UTF-8', $cgi->param('deseu')),'\W');
my $dectxtes = encode_entities(decode('UTF-8', $cgi->param('txtes')),'\W');
my $dectxteu = encode_entities(decode('UTF-8', $cgi->param('txteu')),'\W');

# Save data.
$dbh->do("UPDATE videos SET date='".$date."', tites='".$dectites."', titeu='".$dectiteu."', deses='".$decdeses."', deseu='".$decdeseu."', txtes='".$dectxtes."', txteu='".$dectxteu."', id_cat=".$id_cat.$grid." WHERE id=".$cgi->param('id')."");

undef $dbh;
