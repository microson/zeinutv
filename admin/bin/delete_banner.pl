#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use File::Remove 'remove';
use HTML::Template;

my $path = "../../imagenes/";
my $cgi = CGI->new();
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
);
my $template = HTML::Template->new(filename => "../tmpl/results_banner.tmpl");

# Mandar el Content-Type es obligatorio
print $cgi->header(
		-charset=>'UTF-8',
		-type=>'text/html');

if ( -e $path."banner_es.png" ) { remove ($path."banner_es.png"); }
if ( -e $path."banner_eu.png" ) { remove ($path."banner_eu.png"); }
$dbh->begin_work;
$dbh->do("UPDATE options SET value = '' WHERE key='banner_tites'");
$dbh->do("UPDATE options SET value = '' WHERE key='banner_titeu'");
$dbh->commit;
undef $dbh;

$template->param(banner => 'true', eliminar => 'true');

# Mostramos la plantilla
print $template->output;
