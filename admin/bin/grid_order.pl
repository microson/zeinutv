#!/usr/bin/perl

use strict;
use DBI;
use CGI;

my ($text, $hidden, $contenido)='';
my $cgi = CGI->new();
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
);

my $date = $cgi->param('date');

# Mandar el Content-Type es obligatorio
print $cgi->header(
		-charset=>'UTF-8',
		-type=>'text/xml',
		-expire=>'now');

my $sth = $dbh->prepare("SELECT COUNT(grid_order) FROM videos WHERE ticpar = 'si' AND date='".substr($cgi->param('date'),6,4)."-".substr($cgi->param('date'),3,2)."-".substr($cgi->param('date'),0,2)."'");
$sth->execute();
my ($rows)= $sth->fetchrow_array();
$hidden = $rows + 1;
if ($rows == 0) {
  $text = "Primera noticia de la parrilla para el $date.";
} else {
  $text = "Noticia número $hidden de la parrilla para el $date.";
}

$contenido .= "<?xml version='1.0' encoding='UTF-8'?>";
$contenido .= "<data>";
$contenido .= "<hidden>".$hidden."</hidden>";
$contenido .= "<text>".$text."</text>";
$contenido .= "</data>";
print $contenido;
