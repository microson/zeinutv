#!/usr/bin/perl

use strict;
use DBI;
use CGI;

my $cgi = CGI->new();
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
);

my ($symbol,$order) = '';
my $date = $cgi->param('date');

# Mandar el Content-Type es obligatorio
print $cgi->header(
		-charset=>'UTF-8',
		-type=>'text/html');

# Move DOWN
if ($cgi->param('pos') eq 'down') {
  $symbol = '>';
# Move UP
} elsif ($cgi->param('pos') eq 'up') {
  $symbol = '<';
  $order = 'DESC';
}

# Begin transaction
$dbh->begin_work;

# Get my grid_order
my $sth=$dbh->prepare("SELECT grid_order FROM videos WHERE id='".$cgi->param('id')."'");
$sth->execute();
my $m_res = $sth->fetchrow_hashref();
$m_res=$m_res->{grid_order};
$sth->finish;

# Get reciver's grid_order and id
$sth=$dbh->prepare("SELECT grid_order,id FROM videos WHERE date='".$date."' AND ticpar = 'si' AND grid_order $symbol $m_res ORDER BY grid_order $order LIMIT 1");
$sth->execute();
my $a_res = $sth->fetchrow_hashref();
my $a_id = $a_res->{id};
$a_res=$a_res->{grid_order};
$sth->finish;

# Save new grid_orders (mine/reciver).
$dbh->do("UPDATE videos SET grid_order=$a_res WHERE id='".$cgi->param('id')."'");
$dbh->do("UPDATE videos SET grid_order=$m_res WHERE id=$a_id");

$dbh->commit;
undef $dbh;
