#!/usr/bin/perl

use strict;
use DBI;
use File::Copy qw(copy);
use autodie qw(chmod copy);
use HTML::Template;
use CGI;
use Encode;

my $contenido="";
my $errores = "";
my $template = "";
my $path="../../imagenes/";
my $cgi = CGI->new();

# Mandar el Content-Type es obligatorio
print $cgi->header(
		-charset=>'UTF-8',
		-type=>'text/html');

# Action INDEX o sin parámetro "what".
if ($cgi->param('what') eq 'index' || !$cgi->param('what')) {
  $template = HTML::Template->new(filename => "../tmpl/add_banner.tmpl");

# Action ADD.
} elsif ($cgi->param('what') eq 'add') {
  $template = HTML::Template->new(filename => "../tmpl/results_banner.tmpl");
  # Verificamos que los campos obligatorios están.
  if ($cgi->upload('file_es') eq '') {
    $errores.="<br />* Debes elegir un fichero PNG.";
  }
  if ($cgi->upload('file_eu') eq '') {
    $errores.="<br />* Debes elegir un fichero PNG.";
  }
  # Si no hay problema con los campos procesamos los banner
  if ($errores eq '') {
    # Subimos los ficheros
    binmode($cgi->upload('file_es'));
    binmode($cgi->upload('file_eu'));
    my $banner=$path.'banner_es.png';
    copy($cgi->upload('file_es'), $banner);
    chmod 0666, $banner;
    my $banner=$path.'banner_eu.png';
    copy($cgi->upload('file_eu'), $banner);
    chmod 0666, $banner;
    # Guardamos en BD la descripción si el usuario la ha tecleado
    if($cgi->param('tites') ne '' || $cgi->param('titeu') ne ''){
      # Escape all the text.
      my $dectites = decode('UTF-8', $cgi->param('tites'));
      my $dectiteu = decode('UTF-8', $cgi->param('titeu'));
      my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
        {
          AutoCommit  => 1,
          RaiseError  => 1,
          PrintError  => 0,
          ShowErrorStatement  => 1
        }
      );
      $dbh->begin_work;
      $dbh->do("UPDATE options SET value = '".$cgi->param('tites')."' WHERE key = 'banner_tites'");
      $dbh->do("UPDATE options SET value = '".$cgi->param('titeu')."' WHERE key = 'banner_titeu'");
      $dbh->commit;
      undef $dbh;
      # Mostrar resultados
      $template->param(
        banner => 'true',
        tites => $cgi->param('tites'),
        titeu => $cgi->param('titeu')
      );
    } else {
      $template->param(banner => 'true');
    }
  # Si hay errores, los mostramos
  } else {
    $errores='<p>El banner NO se ha guardado : '.$errores.'</p>';
    $template->param(errores=>$errores);
  }
# FIN action ADD
}

# Mostramos la plantilla
print $template->output;
