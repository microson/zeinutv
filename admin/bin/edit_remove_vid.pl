#!/usr/bin/perl

use strict;
use HTML::Template;
use CGI;
use DBI;
use DateTime;

my $template = "";

my $cgi = CGI->new();
my $dt = DateTime->today();
my $dbh = DBI->connect("dbi:SQLite:dbname=../db/ztv.sqlite3","","",
  {
    AutoCommit  => 1,
    RaiseError  => 1,
    PrintError  => 0,
    ShowErrorStatement  => 1
  }
);

# Mandar el Content-Type es obligatorio
print $cgi->header(
		-charset=>'UTF-8',
		-type=>'text/html');

# Action INDEX o sin parámetro "what".
if ($cgi->param('what') eq 'index' || !$cgi->param('what')) {
  # Obtain three dates: today, yesterday and before yesterday.
  my %dates = ( 1 => ['Hoy',$dt->date()], 2 => ['Ayer',$dt->subtract(days=>1)->date()], 3 => ['Anteayer',$dt->subtract(days=>1)->date()]);
  my (@loop1,@loop2) = ();
  foreach my $date (sort keys %dates) {
    my ($text,$date) = @{$dates{$date}};
    my %row = (text => $text, date => $date);
    push(@loop1, \%row);
  }
  # Obtain categories.
  my $sth = $dbh->prepare("SELECT id,name FROM categories");
  $sth->execute();
  while(my $result = $sth->fetchrow_hashref()) {
    my %row = (id_cat => $result->{id}, text => $result->{name});
    push(@loop2, \%row);
  }
  $sth->finish;

  $template = HTML::Template->new(filename => "../tmpl/edit_remove_vid.tmpl");
  $template->param(dates=>\@loop1,cats=>\@loop2); 
}

# End connection
undef $dbh;

# Mostramos la plantilla
print $template->output;
