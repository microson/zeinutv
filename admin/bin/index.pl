#!/usr/bin/perl

use strict;
use HTML::Template;
use CGI;

my $contenido = "";
my $errores = "";
my $template = HTML::Template->new(filename => "../tmpl/index.tmpl");
my $cgi = CGI->new();

# Mandar el Content-Type es obligatorio
print $cgi->header(
		-charset=>'UTF-8',
		-type=>'text/html');
# Set date for today
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
$year += 1900;
$mon += 01;
if ($mon <= 9) { $mon = "0$mon"; }
if ($mday <= 9) { $mday = "0$mday"; }
if ($hour <= 9) { $hour = "0$hour"; }
if ($min <= 9) { $min = "0$min"; }
my $date = "$mday-$mon-$year / $hour:$min"."h";

# Load param
$template->param(date=>$date);

# Mostramos la plantilla
print $template->output;
