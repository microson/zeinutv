function check(action) {
  // Mandatory fields can't be blank!
  var error = '';
  var patt=new RegExp (/^\d{2}-\d{2}-\d{4}/);

  if(action=='edit') var obj=document.form_edit;
  if(action=='add') var obj=document.form_add;
  if(action=='add_banner') var obj=document.form_add_banner;

  if (action == 'add' || action == 'edit') {
    if(obj.date.value=='') error += "* La fecha es obligatoria.\n";
    if(patt.test(obj.date.value)==false) error += "* La fecha no tiene el formato correcto. Debe tener este formato: dd-mm-aaaa.\n"; 
    if(obj.tites.value=='') error += "* El título en castellano es obligatorio.\n";
    if(obj.titeu.value=='') error += "* El título en euskera es obligatorio.\n";
    if(obj.deses.value=='') error += "* La descripción en castellano es obligatoria.\n";
    if(obj.deseu.value=='') error += "* La descripción en castellano es obligatoria.\n";
    if(obj.txtes.value=='') error += "* El texto en castellano es obligatorio.\n";
    if(obj.txteu.value=='') error += "* El texto en euskera es obligatorio.\n";
  } else if (action == 'add_banner') {
    myvar = obj.file_es.value.substring(obj.file_es.value.length-3,obj.file_es.value.length).toLowerCase();
    if(myvar != 'png') {
      alert('Has seleccionado un fichero "'+myvar+'". Por favor, selecciona un fichero "PNG" en su lugar.');
      return false;
    } else {
      myvareu = obj.file_eu.value.substring(obj.file_eu.value.length-3,obj.file_eu.value.length).toLowerCase();
      if(myvareu != 'png') {
        alert('Has seleccionado un fichero "'+myvareu+'". Por favor, selecciona un fichero "PNG" en su lugar.');
        return false;
      }
    }
  }

  // Actions for ADD
  if (action == 'add') {
    if(obj.file_es.value=='') error += "* Debes seleccionar un fichero de vídeo en español.\n";
    if(obj.file_eu.value=='') error += "* Debes seleccionar un fichero de vídeo en euskera.\n";
    if (error != '') {
      alert (error);
      return false;
    }
    myvar = obj.file_es.value.substring(obj.file_es.value.length-3,obj.file_es.value.length).toLowerCase();
    if(myvar != 'm4v') {
      alert('Has seleccionado un fichero "'+myvar+'". Por favor, selecciona un fichero "m4v" en su lugar.');
      return false;
    } else {
      myvareu = obj.file_eu.value.substring(obj.file_eu.value.length-3,obj.file_eu.value.length).toLowerCase();
      if(myvareu != 'm4v') {
        alert('Has seleccionado un fichero "'+myvareu+'". Por favor, selecciona un fichero "m4v" en su lugar.');
        return false;
      }
    }
    if(obj.ticres.checked==true && obj.ticpar.checked==true) {
      alert('El vídeo no puede ser el resumen y a la vez pertenecer a la parrilla. Ajusta tu selección, por favor.');
      return false;
    }
  }

  // Actions for EDIT
  if (action == 'edit') {
    if (error != '') {
      alert(error);
      return false;
    } else {
      ajaxFunction('edit_save_data');
    }
  }
}

function vtype() {
  var checked=document.getElementById('ticres').checked;
  if (checked == true) document.getElementById('flipflop').style.display='none';
  if (checked == false) document.getElementById('flipflop').style.display='block';
}
     
function vgrid() {
  var checked=document.getElementById('ticpar').checked;
  if (checked == true) ajaxFunction('grid_order');
  if (checked == false) document.getElementById('ticpar_resul').innerHTML='';
}

function ask(id,type) {
  if(type=='video'){
    set(id);
    return confirm("¿Estás seguro/a de que quieres eliminar este vídeo?");
  } else if(type=='banner'){
    return confirm("¿Estás seguro/a de que quieres eliminar el banner?");
  }
}

function set(id) {
  document.getElementById('id_vid').value=id;
}

function display_element(element,action) {
  if (action=='show') document.getElementById(element).style.display='block';
  if (action=='hide') document.getElementById(element).style.display='none';
}

function go_to(where) {
  if (where=='index') window.location.href="/admin/index.html"
}
