function ajaxFunction(action) {
  var xmlHttp;
  try {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e) {
    // Internet Explorer
    try {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e) {
      try {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch (e) {
        alert("Your browser does not support AJAX!");
        return false;
      }
    }
  }

  // COUNT how many grid news today
  if (action=='grid_order') {
    xmlHttp.onreadystatechange=function() {
      if(xmlHttp.readyState==4) {
        var xmlDoc=xmlHttp.responseXML.documentElement;
        document.getElementById('ticpar_resul').innerHTML = xmlDoc.getElementsByTagName('text')[0].childNodes[0].nodeValue;
        document.getElementById('nn').value = xmlDoc.getElementsByTagName('hidden')[0].childNodes[0].nodeValue;
        document.getElementById('upload').disabled=false;
      }
      if(xmlHttp.readyState==1) {
        document.getElementById('upload').disabled=true;
        document.getElementById('ticpar_resul').innerHTML="<img src='/admin/js/b-loading-mini.gif' />";
      }
    }

    var url = "/admin/bin/grid_order.cgi";
    var params = "date="+document.getElementById('date').value;

    xmlHttp.open("GET",url+"?"+params,true);
    xmlHttp.send(null);
  }

  // LOAD news
  if (action=='load_news') {
    xmlHttp.onreadystatechange=function() {
      if(xmlHttp.readyState==4) {
        document.getElementById('show').innerHTML=xmlHttp.responseText;
      }
      if(xmlHttp.readyState==1) {
        document.getElementById('show').innerHTML="<img src='/admin/js/b-loading.gif' />";
	display_element('fs_edit','hide');
      }
    }

    var url = "/admin/bin/load_news.cgi";
    var index = document.getElementById('days').selectedIndex;
    var option = document.getElementsByTagName('option');
    var params = "date="+option[index].value;
    if (option[index].value=='choose') {
      document.getElementById('show').innerHTML='';
      document.getElementById('edit').innerHTML='';
    } else {
      xmlHttp.open("GET",url+"?"+params,true);
      xmlHttp.send(null);
    }
  }

  // DELETE new
  if (action=='delete_new') {
    var id=document.getElementById('id_vid').value;
    var patt=new RegExp (/^\d{2}-\d{2}-\d{4}/);
    xmlHttp.onreadystatechange=function() {
      if(xmlHttp.readyState==4) {
        ajaxFunction("load_news");
      }
      if(xmlHttp.readyState==1) {
	if (patt.test(id)==true) {
          document.getElementById('res').innerHTML="<img src='/admin/js/b-loading.gif' />";
	} else {
          document.getElementById('new_'+id).innerHTML="<img src='/admin/js/b-loading.gif' />";
	}
      }
    }

    var url = "/admin/bin/delete_new.cgi";
    var params = "id="+id;

    xmlHttp.open("GET",url+"?"+params,true);
    xmlHttp.send(null);

  }

  // MOVE UP new
  if (action=='moveup_new') {
    var id=document.getElementById('id_vid').value;
    xmlHttp.onreadystatechange=function() {
      if(xmlHttp.readyState==4) {
        ajaxFunction("load_news");
      }
      if(xmlHttp.readyState==1) {
        document.getElementById('new_'+id).innerHTML="<img src='/admin/js/b-loading.gif' />";
      }
    }
    var url = "/admin/bin/move_new.cgi";
    var index = document.getElementById('days').selectedIndex;
    var option = document.getElementsByTagName('option');
    var params = "id="+id+"&pos=up&date="+option[index].value;
    xmlHttp.open("GET",url+"?"+params,true);
    xmlHttp.send(null);
  } 

  // MOVE DOWN new
  if (action=='movedown_new') {
    var id=document.getElementById('id_vid').value;
    xmlHttp.onreadystatechange=function() {
      if(xmlHttp.readyState==4) {
        ajaxFunction("load_news");
      }
      if(xmlHttp.readyState==1) {
        document.getElementById('new_'+id).innerHTML="<img src='/admin/js/b-loading.gif' />";
      }
    }
    var url = "/admin/bin/move_new.cgi";
    var index = document.getElementById('days').selectedIndex;
    var option = document.getElementsByTagName('option');
    var params = "id="+id+"&pos=down&date="+option[index].value;
    xmlHttp.open("GET",url+"?"+params,true);
    xmlHttp.send(null);
  }

  // EDIT SHOW data
  if (action=='edit_show_data') {
    xmlHttp.onreadystatechange=function() {
      if(xmlHttp.readyState==4) {
	display_element('fs_edit','show');
	window.location.href = "#form_edit_href";
        var json=jQuery.parseJSON( xmlHttp.responseText );
        jQuery('#id_vid').val(json.id);
        jQuery('#date').val(json.date);
        jQuery('#tites').val(json.tites.replace(/\\'/g,'\'').replace(/\\"/g,'"'));
        jQuery('#titeu').val(json.titeu.replace(/\\'/g,'\'').replace(/\\"/g,'"'));
        jQuery('#deses').val(json.deses.replace(/\\'/g,'\'').replace(/\\"/g,'"'));
        jQuery('#deseu').val(json.deseu.replace(/\\'/g,'\'').replace(/\\"/g,'"'));
        jQuery('#txtes').val(json.txtes.replace(/\\'/g,'\'').replace(/\\"/g,'"'));
        jQuery('#txteu').val(json.txteu.replace(/\\'/g,'\'').replace(/\\"/g,'"'));
	jQuery('#cats').val(json.id_cat);
	if (json.ticpar == 'si') {
	  jQuery('#ticpar').prop('checked',true);
	}
      }
    }
    var url = "/admin/bin/edit_show_data.cgi";
    var id=document.getElementById('id_vid').value;
    var params = "id="+id;
    xmlHttp.open("GET",url+"?"+params,true);
    xmlHttp.send(null);
  }

  // EDIT SAVE data
  if (action=='edit_save_data') {
    xmlHttp.onreadystatechange=function() {
      if(xmlHttp.readyState==4) {
	display_element('fs_edit','hide');
	document.getElementById('status').innerHTML="Datos guardados correctamente.";
	ajaxFunction('load_news');
	var t=setTimeout("document.getElementById('status').innerHTML=''",2000);
      }
      if(xmlHttp.readyState==1) {
        document.getElementById('status').innerHTML="<img src='/admin/js/b-loading.gif' />";
      }
    }
    var url='/admin/bin/edit_save_data.cgi';
    var id=document.getElementById('id_vid').value;
    if (document.getElementById('ticpar').checked==false) {
      document.getElementById('ticpar').value='no';
    }
    var params="id="+id+"&date="+document.getElementById('date').value+"&tites="+document.getElementById('tites').value+"&titeu="+document.getElementById('titeu').value+"&deses="+document.getElementById('deses').value+"&deseu="+document.getElementById('deseu').value+"&txtes="+document.getElementById('txtes').value+"&txteu="+document.getElementById('txteu').value+"&id_cat="+document.getElementById('cats').selectedIndex+"&ticpar="+document.getElementById('ticpar').value;
    xmlHttp.open("POST",url,true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.setRequestHeader("Content-length", params.length);
    xmlHttp.setRequestHeader("Connection", "close");
    xmlHttp.send(params);
  }

}
