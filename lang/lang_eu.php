<?php
// lang/lang_eu.php
$lang = array(
    'IDIOMA' => 'eu',
    'KEYWORDS' => 'berriak, zeinu hizkuntza',
    'DESCRIPTION' => 'Berriak zeinu hizkuntzan',
    'TITULO' => 'zeinu.tv :: berriak zeinu hizkuntzan',
    'HOJA_ESTILO' => '/estilo/eu.css',
    'LOGOTIPO_ALT' => 'Zeinutv_ko logoa',
    'TITULO_H2' => 'telebista zeinu hizkuntzan - BETA',
    'URL_INICIO' => '/eus',        
    'URL_EUSKADI' => '/eus/euskadi',    
    'URL_ESPANA' => '/eus/espainia',
    'URL_MUNDO' => '/eus/mundua',
    'URL_DEPORTES' => '/eus/kirolak',
    'URL_OCIO' => '/eus/aisia',
    'URL_COMUNIDAD' => '/eus/komunitatea',
    'MENU_INICIO' => 'hasiera',
    'MENU_EUSKADI' => 'euskadi',
    'MENU_ESPANA' => 'espainia',
    'MENU_MUNDO' => 'mundua',
    'MENU_DEPORTES' => 'kirolak',
    'MENU_OCIO' => 'aisia eta kultura',
    'MENU_COMUNIDAD' => 'gor komunitatea',
    'TITULO_NOTICIAS' => 'azken berriak',    
    'TITULO_ARCHIVO' => 'artxiboa',
    'SELECCIONAR_FECHA' => 'Mesedez, aukeratu egutegiko data',
    'AVISO_LEGAL' => 'Ohar legala',
    'PUBLICIDAD' => 'Publizitatea',
    'ERROR_VIDEOS' => 'Ez dago bideorik!'
);
?>
