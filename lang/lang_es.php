<?php
// lang/lang_es.php
$lang = array(
    'IDIOMA' => 'es',
    'KEYWORDS' => 'noticias, lengua de signos',
    'DESCRIPTION' => 'Noticias en Lengua de Signos',
    'TITULO' => 'zeinu.tv :: noticias en lengua de signos',
    'HOJA_ESTILO' => '/estilo/es.css',
    'LOGOTIPO_ALT' => 'Logotipo de zeinutv',
    'TITULO_H2' => 'televisión en lengua de signos - BETA',
    'URL_INICIO' => '/esp',        
    'URL_EUSKADI' => '/esp/euskadi',    
    'URL_ESPANA' => '/esp/espana',
    'URL_MUNDO' => '/esp/mundo',
    'URL_DEPORTES' => '/esp/deportes',
    'URL_OCIO' => '/esp/ocio',
    'URL_COMUNIDAD' => '/esp/comunidad',
    'MENU_INICIO' => 'inicio',
    'MENU_EUSKADI' => 'euskadi',
    'MENU_ESPANA' => 'españa',
    'MENU_MUNDO' => 'el mundo',
    'MENU_DEPORTES' => 'deportes',
    'MENU_OCIO' => 'ocio y cultura',
    'MENU_COMUNIDAD' => 'comunidad sorda',
    'TITULO_NOTICIAS' => 'últimas noticias',
    'TITULO_ARCHIVO' => 'archivo',
    'SELECCIONAR_FECHA' => 'Por favor, seleccione una fecha del calendario',
    'AVISO_LEGAL' => 'Aviso legal',
    'PUBLICIDAD' => 'Publicidad',
    'ERROR_VIDEOS' => '&iexcl;No se ha encontrado ningún vídeo!'
);
?>
