<?php

// Comprobar que el usuario ha introducido un idioma válido
if ($_GET['lang'] == 'es' or $_GET['lang'] == 'eu' ) {
  // Incluir constantes
  include 'modulos/consts.php';

  // Definimos la variable del idioma
  $user_lang = $_GET['lang'];

  // Incluimos la plantilla con el idioma
  include 'lang/lang_' . basename($user_lang) . '.php';

  // Incluimos los modulos a traducir
  $cabecera = file_get_contents('modulos/cabecera.php');
  $menu = file_get_contents('modulos/menu.php');
  $pie = file_get_contents('modulos/pie.php');

  // Create an empty array for the language variables
  $vars = array();

  // Scroll through each variable
  foreach($lang as $key => $value) {
    // Turn 'THIS' to '{THIS}'
    $vars['{' . $key . '}'] = $value;
  }

  // Finally convert the strings
  $cabecera = strtr($cabecera, $vars);
  $menu = strtr($menu, $vars);
  $pie = strtr($pie, $vars);

  // Cargamos los módulos
  include ("modulos/lista_videos.php");	
  include ("modulos/javascript.php");	
  print $cabecera;
  include ('modulos/publicidad.php');
  print $menu;
  include ("modulos/video.php");
  include ("modulos/noticias.php");
  print $pie;        
} else {
  // Si el idioma seleccionado no es válido se imprime la portada
  $html = file_get_contents('index.html');
  print $html;
}
?>
