      <div id="cab">
        <img src="/imagenes/logo-{IDIOMA}.jpg" alt="{LOGOTIPO_ALT}" />
        <h2>{TITULO_H2}</h2>
        <ul>
          <li class="sec"><a href="{URL_INICIO}">{MENU_INICIO}</a></li>
          <li class="sec"><a href="{URL_EUSKADI}">{MENU_EUSKADI}</a></li>
          <li class="sec"><a href="{URL_ESPANA}">{MENU_ESPANA}</a></li>
          <li class="sec"><a href="{URL_MUNDO}">{MENU_MUNDO}</a></li>
          <li class="sec"><a href="{URL_DEPORTES}">{MENU_DEPORTES}</a></li>
          <li class="sec"><a href="{URL_OCIO}">{MENU_OCIO}</a></li>
          <li class="sec"><a href="{URL_COMUNIDAD}">{MENU_COMUNIDAD}</a></li>
        </ul>
        <div id="fhi"></div>
        <div class="cie"></div>
      </div>
      <div id="cue">
