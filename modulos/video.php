        <div id="vid">
          <div id="ply">
            <div id="jp_container_1" class="jp-video">
              <div class="jp-type-single">
                <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                <div class="jp-gui">
                  <div class="jp-video-play">
                    <a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
                  </div>
                  <div class="jp-interface">
                    <div class="jp-progress">
                      <div class="jp-seek-bar">
                        <div class="jp-play-bar"></div>
                      </div>
                    </div>
                    <div class="jp-current-time"></div>
                    <div class="jp-duration"></div>
                    <div class="jp-controls-holder">
                      <ul class="jp-controls">
                        <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
                        <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
                        <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
                        <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
                        <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
                      </ul>
                      <div class="jp-volume-bar">
                        <div class="jp-volume-bar-value"></div>
                      </div>
                      <ul class="jp-toggles">
                        <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
                        <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
                        <li><a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen">full screen</a></li>
                        <li><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a></li>
                      </ul>
                    </div>
                    <?php
                    if(!isset($cat) && isset($video_resumen)){
                      echo("<span id='mostrador'><img src='/imagenes/plus.png' alt='lista'/></span>");
                    }
                    ?>
                  </div>
                </div>
                <div class='jp-playlist jp-shuffle-off'>
                  <ul>
                    <li></li>
                  </ul>
                </div>
                <div class='jp-no-solution'>
                  <span>Necesitas actualizar</span>
                  Para reproducir las noticias necesitas o actualizar el navegador o actualizar tu versión del <a href='http://get.adobe.com/flashplayer/' target='_blank'>Plugin de Flash</a>.
                </div>
              </div>
            </div>
          </div>
          <div id='txt'>
          <?php 
            include "modulos/Parsedown.php";
            if (isset($lista_videos)) {
              foreach ($lista_videos as $video) {
                $titulo_video = $video['titulo'];
                $descripcion_video = Parsedown::instance()->parse($video['descripcion']);
                $texto_video = Parsedown::instance()->parse(html_entity_decode($video['texto'],ENT_QUOTES,'UTF-8'));
                echo "
                  <h1>$titulo_video</h1>
                  <h2><em>$descripcion_video</em></h2><p class='new'>$texto_video</p>
                "; 
              }
            }
          ?>
          </div>
        </div>
