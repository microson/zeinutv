<?php
//Esta función busca archivos según una expresión regular. 
//Cada vez que encuentra un archivo hace una llamada a la función $callback, pasándole como parámetro el nombre del archivo.
function find_files($path, $pattern, $callback) {
  $path = rtrim(str_replace('\\', '/', $path), '/') . '/';
  $matches = Array();
  $entries = Array();
  $dir = dir($path);
  while (false !== ($entry = $dir->read())) {
    $entries[] = $entry;
  }
  $dir->close();
  foreach ($entries as $entry) {
    $fullname = $path . $entry;
    if ($entry != '.' && $entry != '..' && is_dir($fullname)) {
      find_files($fullname, $pattern, $callback);
    } else if (is_file($fullname) && preg_match($pattern, $entry)) {
      call_user_func($callback, $fullname);
    }
  }
}

//Funciones callback que analizan el resultado devuelto por la función de búsqueda
function busqueda_ano($filename) {
  global $ano;
  if (substr ($filename, '21', '4') > $ano) $ano = substr ($filename, '21', '4');
}
function busqueda_mes($filename) {
  global $mes;
  if (substr ($filename, '18', '2') > $mes) $mes = substr ($filename, '18', '2');
}
function busqueda_dia($filename) {
  global $dia;
  if (substr ($filename, '15', '2') > $dia) {
    $dia = substr ($filename, '15', '2');
    if(!isset($_GET['id'])){
      global $video_resumen;
      $video_resumen = $filename;
    }
  }
}

$diasresumen_mes_actual = array();

function busqueda_calendario_mes_actual($filename) {
  $diatemp = substr ($filename, '15', '2');
  global $diasresumen_mes_actual;
  array_push ($diasresumen_mes_actual, $diatemp);
}

$diasresumen_mes_pasado = array();

function busqueda_calendario_mes_pasado($filename) {
  $diatemp = substr ($filename, '15', '2');
  global $diasresumen_mes_pasado;
  array_push ($diasresumen_mes_pasado, $diatemp);
}

//Inicializamos las variables de la fecha
$dia = 0;
$mes = 0;
$ano = 0;

//Buscamos el año
$regex = "/-...._" . $user_lang . ".m4v$/";
find_files('videos/berriak', $regex, 'busqueda_ano');

//Buscamos el mes
$regex = '/-' . $ano . "_" . $user_lang . ".m4v$/";
find_files('videos/berriak', $regex, 'busqueda_mes');

//Buscamos el dia
$regex = '/' . $mes . '-' . $ano . "_" . $user_lang . ".m4v$/";
find_files('videos/berriak', $regex, 'busqueda_dia');

//Salida
$fecha = $ano . "-" . $mes . "-" . $dia;

//FUNCIONES PARA EL CALENDARIO DEL ARCHIVO. Sacar un array con los días en los que existen vídeos resumen.
//Buscamos todos los días del mes actual
$regex = '/' . $mes . '-' . $ano . "_" . $user_lang . ".m4v$/";
find_files('videos/berriak', $regex, 'busqueda_calendario_mes_actual');

$mes_pasado = $mes -1;
$regex = '/' . $mes_pasado . '-' . $ano . "_" . $user_lang . ".m4v$/";
find_files('videos/berriak', $regex, 'busqueda_calendario_mes_pasado');

// FUNCIONES PARA SACAR LA LISTA DE VIDEOS DE LA BASE DE DATOS

// Si solmente queremos ver una noticia en concreto
// no necesitamos toda la lista
if (isset($_GET['id']) && preg_match("/^[0-9]+$/",$_GET['id'])){
  $consulta = "SELECT * FROM videos WHERE id = ".$_GET['id']."";
} else {
// La lista de vídeos
  if (isset($_GET['cat'])){$cat = $_GET['cat'];}
  // Si la categoría es del rango 1-6:
  if(isset ($cat) && $cat > 0 && $cat < 7) {
    $consulta = "SELECT * FROM videos WHERE id_cat = $cat ORDER BY id DESC";
  // Si la categoría no está definida, sacamos los vídeos de la portada
  } else {
    $consulta = "SELECT * FROM videos WHERE ticpar ='si' AND date = '" . $fecha . "' ORDER BY grid_order";
  }
}
// Noticias del día: realizamos la consulta a la base de datos y guardamos la lista de videos en el array "lista_videos"
$dbh = new PDO(constant('DB')); 
$i = 0;
foreach ($dbh->query($consulta) as $row){
  $lista_videos[$i]['id'] = $row['id'];
  $lista_videos[$i]['date'] = $row['date'];
  $lista_videos[$i]['duration'] = $row['duration'];
  $lista_videos[$i]['id_cat'] = $row['id_cat'];
  if ($user_lang == 'es'){
    $lista_videos[$i]['titulo'] =  html_entity_decode($row['tites'],ENT_QUOTES,'UTF-8'); # SUYCCOMHACK: Se decodifica por el problema comentado por Edu respecto a Chrome y Windows.
    $lista_videos[$i]['descripcion'] =  $row['deses'];
    $lista_videos[$i]['texto'] =  $row['txtes'];
  }else if ($user_lang == 'eu'){
    $lista_videos[$i]['titulo'] =  html_entity_decode($row['titeu'],ENT_QUOTES,'UTF-8'); # SUYCCOMHACK: Se decodifica por el problema comentado por Edu respecto a Chrome y Windows.
    $lista_videos[$i]['descripcion'] =  $row['deseu'];
    $lista_videos[$i]['texto'] =  $row['txteu'];
  }
  $i ++;
}

// Archivo de noticias: solamente realizamos la consulta si estamos visualizando el index: las categorías
// no renderizan el archivo.
if (!isset ($cat)) {
  if (isset($_GET['archivo'])){
    $fecha_archivo = substr($_GET['archivo'], 0,4) . "-" . substr($_GET['archivo'], 4,2) . "-" . substr($_GET['archivo'], 6);
    $consulta = "SELECT * FROM videos WHERE ticpar ='si' AND date = '" . $fecha_archivo . "' ORDER BY grid_order";
    foreach ($dbh->query($consulta) as $row){
      $lista_videos_archivo[$i]['id'] = $row['id'];
      $lista_videos_archivo[$i]['date'] = $row['date'];
      $lista_videos_archivo[$i]['duration'] = $row['duration'];
      $lista_videos_archivo[$i]['id_cat'] = $row['id_cat'];
      if ($user_lang == 'es'){
        $lista_videos_archivo[$i]['titulo'] = html_entity_decode($row['tites'],ENT_QUOTES,'UTF-8'); # SUYCCOMHACK: Se decodifica por el problema comentado por Edu respecto a Chrome y Windows. 
        $lista_videos_archivo[$i]['descripcion'] =  $row['deses'];
        $lista_videos_archivo[$i]['texto'] =  $row['txtes'];
      }else if ($user_lang == 'eu'){
        $lista_videos_archivo[$i]['titulo'] = html_entity_decode($row['titeu'],ENT_QUOTES,'UTF-8'); # SUYCCOMHACK: Se decodifica por el problema comentado por Edu respecto a Chrome y Windows.
        $lista_videos_archivo[$i]['descripcion'] =  $row['deseu'];
        $lista_videos_archivo[$i]['texto'] =  $row['txteu'];
      }
      $i ++;
    }
  }
}
?>
