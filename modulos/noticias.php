<?php
  // Traducimos e imprimimos la cabecera del módulo "noticias"
  $cabecera_noticias = "
    <div class='tabber' >
    <div class='tabbertab' title='{TITULO_NOTICIAS}'>
  ";
  $cabecera_noticias = strtr($cabecera_noticias, $vars);
  print $cabecera_noticias;
  // Renderizamos las cajitas para cada noticia a la derecha
  if (isset($lista_videos)){
    foreach ($lista_videos as $row){
      // Sacamos las variables de cada registro
      $id =  $row['id'];
      $date =  $row['date'];
      $duration =  $row['duration'];
      $id_cat = $row['id_cat'];
      $titulo =  html_entity_decode($row['titulo'],ENT_QUOTES,'UTF-8');
      $titulo_cortado = mb_substr($titulo,0,45,'UTF-8').'...';
      $pos = strrpos($titulo_cortado, " ");
      $descripcion =  html_entity_decode($row['descripcion'],ENT_QUOTES,'UTF-8');
      $descripcion = mb_substr($descripcion,0,37,'UTF-8').'...';
      $descripcion = str_replace("<br />", "", $descripcion);
      $descripcion = preg_replace("[\n\r]", "\t", $descripcion);
      $texto =  $row['texto'];
      // Renderizamos cada cajita :)
      echo "
      <div id='$id' data-cat='$id_cat' class='ite' onclick='hide_menu_box();'>
        <div class='img'>
          <img src='/videos/berriak/" . $id . "_" . $user_lang . ".png' alt='$titulo' title='$titulo' />
        </div>
        <div class='not'>
          <p class='news_header'>$titulo_cortado<br /><span>$titulo</span></p>
          <p class='det'>$descripcion<br />$duration min.</p>
          <p class='desc'>".$row['descripcion']."</p>
          <p class='txt'>$texto</p>";
          if ($user_lang == 'es'){
            $fecha_formateada = substr($date,8) . "/" . substr($date,5,2) . "/" . substr($date,0,4);
            echo "<p class='det_fecha'>$fecha_formateada</p>";
          }else{
             $fecha_formateada = substr($date,0,4) . "/" . substr($date,5,2) . "/" . substr($date,8);
             echo "<p class='det_fecha'>$fecha_formateada</p>";
          }
        echo "
            </div>
          </div>
          <div class='cie'></div>
        ";
    }
  } else {
    $mensaje_error = "
      <div id='no-video'>
        <p><img src='/imagenes/error.png' alt='Error' /></p>
        <p class='no-video'>{ERROR_VIDEOS}</p>
      </div>
    ";
    $mensaje_error = strtr($mensaje_error,$vars);
    print $mensaje_error;
  }
?>
</div>
<?php 
  // Si estamos en una categoría no imprimimos las pestañas
  if (isset ($cat) && $cat > 0 && $cat < 7){}else{
  // Título archivo 
  if (isset($_GET['archivo'])){
    $cabecera_archivo = "<div class='tabbertab tabbertabdefault' title='{TITULO_ARCHIVO}'>";
  }else{      	
    $cabecera_archivo = "<div class='tabbertab' title='{TITULO_ARCHIVO}'>";
  }
  $cabecera_archivo = strtr($cabecera_archivo,$vars);
  print $cabecera_archivo;        
?>

<?php
  // CALENDARIO	
  include "modulos/phpcalendar.php";
  $time = time();
  if ($user_lang=='es'){
    setlocale(LC_ALL,'es_ES.UTF-8');
  }else{
    setlocale(LC_ALL,'eu_ES.UTF-8');
  }
  $enlaces_dias_mes_actual = array();
  $enlaces_dias_mes_pasado = array();
  if ($user_lang == 'es'){
    $idioma_usuario = 'esp';
  }else{
    $idioma_usuario = 'eus';
  }
  $ano = substr ($fecha,0,4);
  // STRINGs que se utilizan para generar las URLs de los días con vídeo resumen
  $mes = substr ($fecha,5,2);
  $mes_pasado = $mes - 1;
  if($mes_pasado < 10){
    $mes_pasado = "0" . $mes_pasado;
  }
  foreach ($diasresumen_mes_actual as $dia){
    $dia_int = $dia + 0;
    $fechatemp =  $ano . $mes . $dia;
    $dia_url = array("/$idioma_usuario/archivo/$fechatemp",'dia-especial');
    $enlaces_dias_mes_actual[$dia_int] =  $dia_url;    		
  }
  foreach ($diasresumen_mes_pasado as $dia){
    $dia_int = $dia + 0; 
    $fechatemp =  $ano . $mes_pasado . $dia;
    $dia_url = array("/$idioma_usuario/archivo/$fechatemp",'dia-especial');
    $enlaces_dias_mes_pasado[$dia_int] =  $dia_url;    		
  }
  echo generate_calendar($ano, $mes, $enlaces_dias_mes_actual, 1, NULL, 1, NULL);
  echo generate_calendar($ano, $mes_pasado, $enlaces_dias_mes_pasado, 1, NULL, 1,NULL);
?>

<?php
// En el caso de que se haya pasado la variable $lista_videos_archivo, renderizamos el archivo de ese dia
if (isset($lista_videos_archivo)){
  if ($user_lang == 'es'){
    $fecha_formateada = substr($_GET['archivo'],6) . "-" . substr($_GET['archivo'],4,2) . "-" . substr($_GET['archivo'],0,4);
    echo "<div class='cie'></div><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Archivo de vídeos del día $fecha_formateada</h3>";
  }else{
    $fecha_formateada = substr($_GET['archivo'],0,4) . "-" . substr($_GET['archivo'],4,2) . "-" . substr($_GET['archivo'],6);
    echo "<div class='cie'></div><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$fecha_formateada"."ko bideoen artxiboa</h3>";
  }
  foreach ($lista_videos_archivo as $row){
    // Sacamos las variables de cada registro
    $id =  $row['id'];
    $date =  $row['date'];
    $duration =  $row['duration'];
    $id_cat = $row['id_cat'];
    $titulo =  html_entity_decode($row['titulo'],ENT_QUOTES,'UTF-8');
    $titulo_completo = html_entity_decode($row['titulo'],ENT_QUOTES,'UTF-8');
    $titulo_cortado = mb_substr($titulo,0,45,'UTF-8').'...';
    $pos = strrpos($titulo_cortado, " ");
    $descripcion =  html_entity_decode($row['descripcion'],ENT_QUOTES,'UTF-8');
    $descripcion = mb_substr($descripcion,0,37,'UTF-8').'...';
    $pos = strrpos($descripcion," ");
    $descripcion = str_replace("<br />", "", $descripcion);
    $descripcion = preg_replace("[\n\r]", "\t", $descripcion);
    $texto =  $row['texto'];
    // Renderizamos cada cajita :)
    echo "
      <div id='$id' data-cat='$id_cat' class='ite' onclick='hide_menu_box();'>
        <div class='img'>
	  <img src='/videos/berriak/" . $id . "_" . $user_lang . ".png' alt='$titulo' title='$titulo' />
	</div>
	<div class='not'>
          <p class='news_header'>$titulo_cortado<br /><span>$titulo_completo</span></p>
	  <p class='det'>$descripcion<br />$duration min.</p>
          <p class='desc'>".$row['descripcion']."</p>
          <p class='txt'>$texto</p>
	</div>
      </div>
      <div class='cie'></div>
    ";
  }
}else{ 
  $mensaje_error ="<div class='cie'></div><h4>&nbsp;&nbsp;&nbsp;&nbsp;{SELECCIONAR_FECHA}</h4>"; 
  $mensaje_error = strtr($mensaje_error,$vars);
  print $mensaje_error;
}
?>

<div class='cie'></div>

<?php
}
?>
  </div>        
</div>	
<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
