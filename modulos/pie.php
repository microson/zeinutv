      <div id='pie'>
        <div id='logos'>
        	<a href='http://www.bbk.es/' title='BBK'>
        	  <img src='/imagenes/bbk_{IDIOMA}.jpg' title='BBK' />
        	</a>
        	<a href='http://www.eitb.com/' title='EITB'>
        	  <img src='/imagenes/eitb_{IDIOMA}.jpg' title='EiTB' />
        	</a>
        	<a href='http://www.euskal-gorrak.org/' title='Euskalgorrak'>
        	  <img src='/imagenes/eg_{IDIOMA}.jpg' title='Euskalgorrak' />
        	</a>
        	<a href='http://www.euskadi.net/' title='EJ-GV'>
        	  <img src='/imagenes/ej-gv_{IDIOMA}.jpg' title='EJ-GV' />
        	</a>
        </div>
        <div id='info'>
          <a href='/aviso_legal_{IDIOMA}.html' title='Aviso legal'>
            {AVISO_LEGAL}
          </a>
          &nbsp;&nbsp;&nbsp;
          <a href='/publicidad_{IDIOMA}.html' title='Contratar publicidad'>
            {PUBLICIDAD}
          </a>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>
