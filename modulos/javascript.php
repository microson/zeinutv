<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
  <head>
    <link rel="icon" href="/imagenes/favicon.ico" type="image/x-ico" />
    <link rel="shortcut icon" href="/imagenes/favicon.ico" />
    <link rel="icon" type="image/gif" href="/imagenes/animated_favicon.gif" />
    <link rel="stylesheet" type="text/css" href="/estilo/common.css" />
    <link rel="stylesheet" type="text/css" href="/estilo/tabber.css" />
    <link rel='stylesheet' type='text/css' href='/player/skins/midnight_black/jplayer.midnight.black.css' />
    <?php
      if ($user_lang == 'eu'){
        echo "<link rel='stylesheet' type='text/css' href='/estilo/eu.css' />";
	echo "<!--[if lte IE 7]><link rel='stylesheet' type='text/css' href='/estilo/common_IE.css'><link rel='stylesheet' type='text/css' href='/estilo/eu_IE.css' /><![endif]-->";
      }else{
        echo "<link rel='stylesheet' type='text/css' href='/estilo/es.css' />";
        echo "<!--[if lte IE 7]><link rel='stylesheet' type='text/css' href='/estilo/common_IE.css' /><link rel='stylesheet' type='text/css' href='/estilo/es_IE.css' /><![endif]-->";
    }?>

    <!-- Some other stuff -->
    <script type="text/javascript" src="/javascript/markdown.min.js"></script>

    <!-- jQuery and jPlayer -->
    <script type="text/javascript" src="/javascript/jquery.min.js"></script>
    <script type="text/javascript" src="/player/jquery.jplayer.min.js"></script>
    <script type='text/javascript' src='/player/add-on/jplayer.playlist.min.js'></script>
    <script type="text/javascript">
      // This removes a stupid box margin appended to the player on certain cirsumstances
      function hide_menu_box(){
        $('#jp_container_1').height('335px');
      }
      $(document).ready(function(){
        // jPlayer
        var ztvplist = new jPlayerPlaylist({
          jPlayer: "#jquery_jplayer_1",
          cssSelectorAncestor: "#jp_container_1"
        }, [
            <?php
              function plist($noticia){
                global $user_lang;
                $id = $noticia['id'];
                $titulo =  $noticia['titulo'];
                $titulo = str_replace("\"", "'", $titulo);
                $titulo = preg_replace("[\n\r]", "\t", $titulo);
                echo "
                  {
                    title: \"".$titulo."\",
                    m4v: \"".constant('ALMACENAMIENTO_NOTICIAS').$id."_".$user_lang.constant('EXT_MP4')."\",
                    webmv: \"".constant('ALMACENAMIENTO_NOTICIAS').$id."_".$user_lang.constant('EXT_WEBM')."\"
                  },
                ";
              }
              if(!isset($cat) && isset($video_resumen)) {
                if($user_lang == 'es') { $resumen = 'Resumen: ' . $dia . "-" . $mes . "-" . $ano; } else { $resumen = 'Laburpena: ' . $fecha; }
                echo "
                  {
                    title: \"" . $resumen . "\",
                    m4v: \"".constant('ALMACENAMIENTO_NOTICIAS').$dia.'-'.$mes.'-'.$ano.'_'.$user_lang.constant('EXT_MP4')."\",
                    webmv: \"".constant('ALMACENAMIENTO_NOTICIAS').$dia.'-'.$mes.'-'.$ano.'_'.$user_lang.constant('EXT_WEBM')."\"
                  },
                ";
              }
              if(isset($lista_videos)) {
                foreach ($lista_videos as $noticia) {
                  plist($noticia);
                }
              }
            ?>
        ], {
          playlistOptions: {
            autoPlay: true,
            enableRemoveControls: false
          },
          swfPath: "/player",
          supplied: "m4v,webmv",
          smoothPlayBar: true,
          keyEnabled: true,
        });
        <!-- End jPlayer -->
        <!-- Events -->
        $("span#mostrador").click(function() {
          if($('.jp-playlist').css('display') == 'none'){
            $('span#mostrador img').attr("src","/imagenes/minus.png");
            $('.jp-playlist').show();
          } else {
            $('span#mostrador img').attr("src","/imagenes/plus.png");
            $('.jp-playlist').hide();
          }
        });
        $('div.ite').click(function() {
          $('.jp-playlist').hide();
          $('#mostrador').hide();
          ztvplist.setPlaylist([
            {
              title: $('p.news_header span', this).html(),
              m4v: '<?php echo constant('ALMACENAMIENTO_NOTICIAS') ?>' + $(this).attr('id') + '_' + '<?php echo $user_lang.constant('EXT_MP4') ?>',
              webmv: '<?php echo constant('ALMACENAMIENTO_NOTICIAS') ?>' + $(this).attr('id') + '_' + '<?php echo $user_lang.constant('EXT_WEBM') ?>'
            }
          ]);
          $('div#txt').html(
            '<h1>' + 
            $('p.news_header span', this).html() + 
            '</h1>' +  
            '<h2>' + 
            markdown.toHTML($('p.desc', this).html())  + 
            '</h2><p class="new">' + 
            markdown.toHTML($('p.txt', this).html()) + 
            '</p>'
          );
        });
        <!-- End events -->
        // Don't show bottom ugly line
        if($('#mostrador').is(':visible')){
          $('#jp_container_1').height('');
        } else {
          hide_menu_box();
        }
      });
    </script>

    <script type="text/javascript" src="/estilo/tabber-minimized.js"></script>
    <script type="text/javascript" src="/javascript/aux_<?php echo $user_lang ?>.js"></script>
